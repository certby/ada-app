.PHONY: docs clean

COMMAND = docker-compose run --rm django-app /bin/bash -c

VERSION = "Added Version $(shell date +%FT%T)"

#all: build test

push:
	git add . && git commit -am$(VERSION) && git push origin prod

docker-build:
	docker-compose down && docker-compose up -d --build
#all: build test
release:
	make push && make build

pull:
	git pull origin prod

docker-run:
	docker-compose up -d

docker-stop:
	docker-compose down

docker-restart:
	docker-compose down && docker-compose up -d

restart:
	chown apache.apache db.sqlite3 && service httpd restart

#migrate:
#	$(COMMAND) 'cd /src; ./manage.py migrate; done'
#
#collectstatic:
#	$(COMMAND) 'cd /src; ./manage.py collectstatic --no-input; done'
#
#check: checksafety checkstyle
#
#test:
#	$(COMMAND) "pip install tox && tox -e test"
#
#checksafety:
#	$(COMMAND) "pip install tox && tox -e checksafety"
#
#checkstyle:
#	$(COMMAND) "pip install tox && tox -e checkstyle"
#
#coverage:
#	$(COMMAND) "pip install tox && tox -e coverage"
#
#clean:
#	rm -rf build
#	rm -rf hello.egg-info
#	rm -rf dist
#	rm -rf htmlcov
#	rm -rf .tox
#	rm -rf .cache
#	rm -rf .pytest_cache
#	find . -type f -name "*.pyc" -delete
#	rm -rf $(find . -type d -name __pycache__)
#	rm .coverage
#	rm .coverage.*
#
#dockerclean:
#	docker system prune -f
#	docker system prune -f --volumes