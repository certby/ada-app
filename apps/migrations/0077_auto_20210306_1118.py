# Generated by Django 2.2.17 on 2021-03-06 08:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0076_documents'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='documents',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Belgeler'},
        ),
    ]
