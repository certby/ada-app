# Generated by Django 2.2.18 on 2021-03-14 11:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0120_auto_20210314_1421'),
    ]

    operations = [
        migrations.AlterField(
            model_name='about',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='about',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='blogs',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='blogs',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='contactform',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='contactform',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='documents',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='documents',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='events',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='events',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='gallery',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='gallery',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='home',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='home',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='museums',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='museums',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='mylogo',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='mylogo',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='news',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='news',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='pages',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='pages',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='places',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='places',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='practicalinformation',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='practicalinformation',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='social',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='social',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='sss',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='sss',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='subscriber',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='subscriber',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='transportation',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='transportation',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
        migrations.AlterField(
            model_name='travelplaces',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi'),
        ),
        migrations.AlterField(
            model_name='travelplaces',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi'),
        ),
    ]
