# Generated by Django 2.2.18 on 2021-03-14 13:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0127_auto_20210314_1619'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='museums',
            name='author',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='head_title',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='head_title_ar',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='head_title_en',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='head_title_ru',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='head_title_tr',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='image',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='images',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='place_room',
        ),
        migrations.RemoveField(
            model_name='museums',
            name='thumbnail',
        ),
    ]
