# Generated by Django 2.1.15 on 2021-07-04 12:34

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0179_auto_20210702_2158'),
    ]

    operations = [
        migrations.AddField(
            model_name='travelplacespage',
            name='content',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='content_ar',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='content_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='content_ru',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='content_tr',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='title',
            field=models.CharField(blank=True, max_length=60, verbose_name='Başlık'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='title_ar',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='Başlık'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='title_en',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='Başlık'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='title_ru',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='Başlık'),
        ),
        migrations.AddField(
            model_name='travelplacespage',
            name='title_tr',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='Başlık'),
        ),
    ]
