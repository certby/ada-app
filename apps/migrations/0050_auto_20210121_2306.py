# Generated by Django 2.2.17 on 2021-01-21 20:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0049_places_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sss',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=50)),
                ('questions', models.CharField(max_length=100)),
                ('answer', models.TextField()),
            ],
            options={
                'verbose_name_plural': 'Sıkça Sorulan Sorular',
                'db_table': 'sss',
                'ordering': ['-created_at'],
            },
        ),
        migrations.AlterModelOptions(
            name='about',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Hakkımızda'},
        ),
        migrations.AlterModelOptions(
            name='blogs',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Bloglar'},
        ),
        migrations.AlterModelOptions(
            name='contact',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'İletişim'},
        ),
        migrations.AlterModelOptions(
            name='events',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Etkinlikler'},
        ),
        migrations.AlterModelOptions(
            name='gallery',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Albümler'},
        ),
        migrations.AlterModelOptions(
            name='home',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Anasayfa'},
        ),
        migrations.AlterModelOptions(
            name='museums',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Müzeler'},
        ),
        migrations.AlterModelOptions(
            name='mylogo',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Logomuz'},
        ),
        migrations.AlterModelOptions(
            name='news',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Haberler'},
        ),
        migrations.AlterModelOptions(
            name='pages',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Sayfalar'},
        ),
        migrations.AlterModelOptions(
            name='places',
            options={'ordering': ['-created_at'], 'verbose_name_plural': 'Gezilecek Yerler'},
        ),
        migrations.AlterModelOptions(
            name='social',
            options={'ordering': ['id'], 'verbose_name_plural': 'Sosyal Medya'},
        ),
        migrations.DeleteModel(
            name='Slider',
        ),
    ]
