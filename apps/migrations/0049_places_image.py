# Generated by Django 2.2.17 on 2021-01-21 19:51

import apps.helper.pathcreater
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0048_auto_20210121_2250'),
    ]

    operations = [
        migrations.AddField(
            model_name='places',
            name='image',
            field=models.FileField(default=1, upload_to=apps.helper.pathcreater.get_file_path),
            preserve_default=False,
        ),
    ]
