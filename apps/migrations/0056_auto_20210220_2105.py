# Generated by Django 2.2.17 on 2021-02-20 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0055_travelplacegallery_travelplaces_travelplaceslider'),
    ]

    operations = [
        migrations.AddField(
            model_name='travelplacegallery',
            name='status',
            field=models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum'),
        ),
        migrations.AddField(
            model_name='travelplaces',
            name='slug',
            field=models.SlugField(default=1, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='travelplaces',
            name='status',
            field=models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum'),
        ),
        migrations.AddField(
            model_name='travelplaceslider',
            name='status',
            field=models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum'),
        ),
    ]
