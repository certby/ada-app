# Generated by Django 2.2.17 on 2021-03-07 15:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0094_auto_20210307_1836'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='about',
            options={'ordering': ['-created_at'], 'verbose_name': 'Hakkımızda', 'verbose_name_plural': 'Hakkımızda'},
        ),
        migrations.AlterModelOptions(
            name='blogs',
            options={'ordering': ['-created_at'], 'verbose_name': 'Blog', 'verbose_name_plural': 'Bloglar'},
        ),
        migrations.AlterModelOptions(
            name='contact',
            options={'ordering': ['-created_at'], 'verbose_name': 'İletişim', 'verbose_name_plural': 'İletişim'},
        ),
        migrations.AlterModelOptions(
            name='contactform',
            options={'ordering': ['-created_at'], 'verbose_name': 'İletişim Formu', 'verbose_name_plural': 'İletişim Formu'},
        ),
        migrations.AlterModelOptions(
            name='departuretimes',
            options={'verbose_name': 'Sefer Saati', 'verbose_name_plural': 'Sefer Saatleri'},
        ),
        migrations.AlterModelOptions(
            name='documents',
            options={'ordering': ['-created_at'], 'verbose_name': 'Belge', 'verbose_name_plural': 'Belgeler'},
        ),
        migrations.AlterModelOptions(
            name='events',
            options={'ordering': ['-created_at'], 'verbose_name': 'Etkinlik', 'verbose_name_plural': 'Etkinlikler'},
        ),
        migrations.AlterModelOptions(
            name='gallery',
            options={'ordering': ['-created_at'], 'verbose_name': 'Albüm', 'verbose_name_plural': 'Albümler'},
        ),
        migrations.AlterModelOptions(
            name='galleryimage',
            options={'verbose_name': 'Galeri', 'verbose_name_plural': 'Galeri'},
        ),
        migrations.AlterModelOptions(
            name='home',
            options={'ordering': ['-created_at'], 'verbose_name': 'Anasayfa', 'verbose_name_plural': 'Anasayfa'},
        ),
        migrations.AlterModelOptions(
            name='museumimage',
            options={'verbose_name': 'Müze Resim', 'verbose_name_plural': 'Müze Resimler'},
        ),
        migrations.AlterModelOptions(
            name='museumplacerooms',
            options={'verbose_name': 'Müze Gezilecek Oda', 'verbose_name_plural': 'Müze Gezilecek Odalar'},
        ),
        migrations.AlterModelOptions(
            name='museums',
            options={'ordering': ['-created_at'], 'verbose_name': 'Müze', 'verbose_name_plural': 'Müzeler'},
        ),
        migrations.AlterModelOptions(
            name='mylogo',
            options={'ordering': ['-created_at'], 'verbose_name': 'Logomuz', 'verbose_name_plural': 'Logomuz'},
        ),
        migrations.AlterModelOptions(
            name='news',
            options={'ordering': ['-created_at'], 'verbose_name': 'Haber', 'verbose_name_plural': 'Haberler'},
        ),
        migrations.AlterModelOptions(
            name='pages',
            options={'ordering': ['-created_at'], 'verbose_name': 'Sayfa', 'verbose_name_plural': 'Sayfalar'},
        ),
        migrations.AlterModelOptions(
            name='places',
            options={'ordering': ['-created_at'], 'verbose_name': 'Gezilecek Yer', 'verbose_name_plural': 'Gezilecek Yerler'},
        ),
        migrations.AlterModelOptions(
            name='social',
            options={'ordering': ['id'], 'verbose_name': 'Sosyal Medya', 'verbose_name_plural': 'Sosyal Medya'},
        ),
        migrations.AlterModelOptions(
            name='sss',
            options={'ordering': ['-created_at'], 'verbose_name': 'Sıkça Sorulan Sorular', 'verbose_name_plural': 'Sıkça Sorulan Sorular'},
        ),
        migrations.AlterModelOptions(
            name='transportation',
            options={'ordering': ['-created_at'], 'verbose_name': 'Ulaşım', 'verbose_name_plural': 'Ulaşım'},
        ),
        migrations.AlterModelOptions(
            name='travelplacegallery',
            options={'verbose_name': 'Gezilecek Yerler Galeri', 'verbose_name_plural': 'Gezilecek Yerler Galeri'},
        ),
        migrations.AlterModelOptions(
            name='travelplaces',
            options={'ordering': ['-created_at'], 'verbose_name': 'Gezilecek Yerler Sayfası', 'verbose_name_plural': 'Gezilecek Yerler Sayfası'},
        ),
        migrations.AlterModelOptions(
            name='travelplaceslider',
            options={'verbose_name': 'Gezilecek Yerler Slider', 'verbose_name_plural': 'Gezilecek Yerler Slider'},
        ),
        migrations.RemoveField(
            model_name='museumplacerooms',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='museumplacerooms',
            name='updated_at',
        ),
    ]
