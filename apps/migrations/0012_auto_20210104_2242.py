# Generated by Django 2.2.17 on 2021-01-04 19:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0011_auto_20210104_2237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='country_code',
            field=models.CharField(max_length=5, null=True),
        ),
    ]
