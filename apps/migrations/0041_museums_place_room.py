# Generated by Django 2.2.17 on 2021-01-20 20:08

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0040_museums_pdf'),
    ]

    operations = [
        migrations.AddField(
            model_name='museums',
            name='place_room',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, verbose_name='Gezilecek Odalar'),
        ),
    ]
