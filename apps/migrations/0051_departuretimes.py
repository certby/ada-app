# Generated by Django 2.2.17 on 2021-02-18 20:53

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0050_auto_20210121_2306'),
    ]

    operations = [
        migrations.CreateModel(
            name='DepartureTimes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.SmallIntegerField(choices=[(1, 'Active'), (2, 'Passive'), (2, 'Pending Approve')], default=2, verbose_name='Durum')),
                ('button_value', models.CharField(blank=True, max_length=100, verbose_name='Buton Adı')),
                ('url', models.CharField(blank=True, max_length=100, verbose_name='Link')),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, verbose_name='İçerik')),
                ('departureTime', ckeditor_uploader.fields.RichTextUploadingField(blank=True, verbose_name='Sefer Saatleri')),
                ('home', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='apps.Home')),
            ],
        ),
    ]
