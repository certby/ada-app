# Generated by Django 2.2.17 on 2021-02-22 20:59

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0068_placerooms_placeroomsgallery'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='placeroomsgallery',
            name='head_title',
        ),
        migrations.AlterField(
            model_name='placeroomsgallery',
            name='image',
            field=ckeditor_uploader.fields.RichTextUploadingField(verbose_name='Slider Resim'),
        ),
    ]
