# Generated by Django 2.1.15 on 2021-07-01 13:23

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0170_auto_20210701_1620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='travelplaces',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, verbose_name='İçerik'),
        ),
        migrations.AlterField(
            model_name='travelplaces',
            name='description_ar',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AlterField(
            model_name='travelplaces',
            name='description_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AlterField(
            model_name='travelplaces',
            name='description_ru',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AlterField(
            model_name='travelplaces',
            name='description_tr',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
    ]
