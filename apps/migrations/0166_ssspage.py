# Generated by Django 2.1.15 on 2021-06-30 21:42

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0165_home_e_ticket_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='SSSPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Kayıt Tarihi')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Güncellenme Tarihi')),
                ('meta_title', models.CharField(default='', max_length=100, verbose_name='Meta Title')),
                ('meta_description', models.TextField(default='', verbose_name='Meta Description')),
                ('other_metas', ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', null=True, verbose_name='Diğer Metalar')),
                ('slug', models.SlugField(max_length=40, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'SSS',
                'verbose_name_plural': 'SSS',
                'db_table': 'sss_page',
                'ordering': ['-created_at'],
            },
        ),
    ]
