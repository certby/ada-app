# Generated by Django 2.1.15 on 2021-03-19 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0133_remove_museums_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='about',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='about',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='about',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='about',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='blogs',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contact',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contactform',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contactform',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contactform',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='contactform',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='departuretimes',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='departuretimes',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='departuretimes',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='departuretimes',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='documents',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='events',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='events',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='events',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='events',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='home',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homepagevideo',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homepagevideo',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homepagevideo',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='homepagevideo',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumimage',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumimage',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumimage',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumimage',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumplacerooms',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumplacerooms',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumplacerooms',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museumplacerooms',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museums',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museums',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museums',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='museums',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='mylogo',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='mylogo',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='mylogo',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='mylogo',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='news',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='news',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='news',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='news',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='pages',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='pages',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='pages',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='pages',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='panoramicmuseum',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='panoramicmuseum',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='panoramicmuseum',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='panoramicmuseum',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformationitem',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformationitem',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformationitem',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='practicalinformationitem',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='social',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='sss',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='sss',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='sss',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='sss',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='subscriber',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='transportation',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='transportation',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='transportation',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='transportation',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplacegallery',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplacegallery',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplacegallery',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplacegallery',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaces',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaces',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaces',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaces',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaceslider',
            name='is_show_ar',
            field=models.BooleanField(default=True, verbose_name='Arapça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaceslider',
            name='is_show_en',
            field=models.BooleanField(default=True, verbose_name='İngilizce Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaceslider',
            name='is_show_ru',
            field=models.BooleanField(default=True, verbose_name='Rusça Gözüksün Mü ?'),
        ),
        migrations.AddField(
            model_name='travelplaceslider',
            name='is_show_tr',
            field=models.BooleanField(default=True, verbose_name='Türkçe Gözüksün Mü ?'),
        ),
    ]
