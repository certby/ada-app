# Generated by Django 2.2.18 on 2021-03-13 09:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0109_auto_20210312_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='home',
            name='opening_time',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Açılış Saati'),
        ),
    ]
