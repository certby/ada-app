# Generated by Django 2.2.18 on 2021-03-12 15:38

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0097_auto_20210312_1836'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='practicalinformation',
            options={'ordering': ['-created_at'], 'verbose_name': 'Pratik Bilgiler', 'verbose_name_plural': 'Pratik Bilgiler'},
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='content_ar',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='content_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='content_ru',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='content_tr',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='İçerik'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='title_ar',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='İsim'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='title_en',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='İsim'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='title_ru',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='İsim'),
        ),
        migrations.AddField(
            model_name='practicalinformation',
            name='title_tr',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='İsim'),
        ),
    ]
