# Generated by Django 2.1.15 on 2021-05-16 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0149_auto_20210516_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='meta_description',
            field=models.TextField(default='', verbose_name='Meta Description'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='meta_keywords',
            field=models.CharField(default='', max_length=100, verbose_name='Meta Keywords'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='meta_title',
            field=models.CharField(default='', max_length=100, verbose_name='Meta Title'),
        ),
    ]
