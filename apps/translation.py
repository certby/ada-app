from modeltranslation.translator import translator, TranslationOptions
from .models import *
from sitetree.models import TreeItem


class BaseTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'head_title')


class HomeTranslationOptions(TranslationOptions):
    fields = ('content', 'content_continue')


class PracticalInformationTranslationOptions(TranslationOptions):
    fields = ('title',)


class TransportationTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'description_title')


class PracticalInformationItemTranslationOptions(TranslationOptions):
    fields = ('label',)


class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'short_description')


class EventsTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'short_description')


class BlogsTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'short_description')


class HomePageVideoTranslationOptions(TranslationOptions):
    fields = ('title', 'button_value')


class SSSTranslationOptions(TranslationOptions):
    fields = ('questions', 'answer')


class LiveSupportTranslationOptions(TranslationOptions):
    fields = ('title',)


class TravelPlacesTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'short_description')


class TravelPlacesPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


class MuseumsTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


class DocumentsTranslationOptions(TranslationOptions):
    fields = ('kvkk', 'kullanim_kosullari', 'acik_riza_beyani', 'gizlilik_politikasi', 'bilgi_toplumu_hizmetleri')


class ContactTranslationOptions(TranslationOptions):
    fields = ('title',)


translator.register(Home, HomeTranslationOptions)
translator.register(Sss, SSSTranslationOptions)
translator.register(Documents, DocumentsTranslationOptions)
translator.register(TravelPlaces, TravelPlacesTranslationOptions)
translator.register(TravelPlacesPage, TravelPlacesPageTranslationOptions)
translator.register(About, BaseTranslationOptions)
translator.register(MyLogo, MuseumsTranslationOptions)
translator.register(News, NewsTranslationOptions)
translator.register(Events, EventsTranslationOptions)
translator.register(Museums, MuseumsTranslationOptions)
translator.register(HomePageVideo, HomePageVideoTranslationOptions)
translator.register(Blogs, BlogsTranslationOptions)
translator.register(Contact, ContactTranslationOptions)
translator.register(PracticalInformation, PracticalInformationTranslationOptions)
translator.register(PracticalInformationItem, PracticalInformationItemTranslationOptions)
translator.register(LiveSupport, LiveSupportTranslationOptions)
translator.register(Transportation, TransportationTranslationOptions)


class TreeItemTranslationOptions(TranslationOptions):
    fields = ('title', 'url',)


translator.register(TreeItem, TreeItemTranslationOptions)
