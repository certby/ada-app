from django.conf.urls import url
from django.contrib.sitemaps.views import sitemap

from . import views
from django.urls import path

from .sitemaps import *
from .views import NewsDetail, EventDetail, BlogDetail, MuseumDetail, PanoramicMuseumDetail, TravelPlacesDetail, \
    QrCodePageDetail

sitemaps = {
    'static': StaticViewSitemap,
    'new': NewsSitemap,
    'event': EventsSitemap,
    'blog': BlogSitemap,
    'travel_places': TravelPlacesSitemap,
    'museum': MuseumsSitemap,
}

urlpatterns = [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}),
    path('', views.home, name='home'),
    # path('hakkimizda', views.about, name='about'),
    path('sss', views.sss, name='sss'),
    path('logomuz', views.my_logo, name='my_logo'),

    path('gezilecek-yerler', views.travel_places_view, name='travel-places'),
    path('galeri', views.gallery, name='gallery'),
    path('ulasim', views.transportation_view, name='transportation'),
    path('duyurular', views.news, name='news'),
    path('kullanim_kosullari', views.kullanim_kosullari, name='kullanim_kosullari'),
    path('gizlilik', views.gizlilik, name='gizlilik'),
    path('bilgi_toplumu_hizmetleri', views.bilgi_toplumu_hizmetleri, name='bilgi_toplumu_hizmetleri'),
    path('kisisel_verilerin_korunmasi_kanunu', views.kisisel_verilerin_korunmasi_kanunu,
         name='kisisel_verilerin_korunmasi_kanunu'),
    path('iletisim', views.contact, name='contact'),
    path('iletisim-formu', views.contact_form_view, name='contact-form'),
    path('aboneler', views.subscriber_view, name='subscriber'),
    path('etkinlik', views.events, name='events'),
    path('panoramic-muze', views.panoramic_museum, name='panoramic_museum'),
    path('haber-detay/<slug:slug>', NewsDetail.as_view(), name="news_detail"),
    path('etkinlik-detay/<slug:slug>', EventDetail.as_view(), name="event_detail"),
    path('blog-detay/<slug:slug>', BlogDetail.as_view(), name="blog_detail"),
    path('gezilecek-yerler/<slug:slug>', TravelPlacesDetail.as_view(), name="travel-places-detail"),
    path('panoramik-muze/<slug:slug>', PanoramicMuseumDetail.as_view(), name="panoramic_museums"),
    path('panoramik-oda/<slug:slug>', PanoramicMuseumDetail.as_view(), name="panoramic_museums"),
    path('<slug:slug>', MuseumDetail.as_view(), name="museum_detail"),
    path('qr/<slug:slug>', QrCodePageDetail.as_view(), name="qrcode_page_detail"),
    path('clear-cache', views.clear_cache),
]
