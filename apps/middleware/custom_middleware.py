from django import http

from django.utils import translation
from apps.models import ACTIVE, SiteLanguages


class CustomMiddleware(object):
    response_gone_class = http.HttpResponseGone
    response_redirect_class = http.HttpResponsePermanentRedirect

    def __init__(self, get_response):
        """
        One-time configuration and initialisation.
        """
        self.get_response = get_response

    def __call__(self, request):
        """
        Code to be executed for each request before the view (and later
        middleware) are called.
        """
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        """
        Called just before Django calls the view.
        """
        return None

    # def process_exception(self, request, exception):
    #     """
    #     Called when a view raises an exception.
    #     """
    #
    #     return self.response_redirect_class("/")

    def process_template_response(self, request, response):
        """
        Called just after the view has finished executing.
        """
        try:
            req_lang = request.LANGUAGE_CODE
            languages = SiteLanguages.objects.filter(status=ACTIVE)

            items = []
            for lang in languages:
                items.append(lang.code)

            if req_lang not in items:
                return self.response_redirect_class("/")

        except Exception:
            pass

        return response
