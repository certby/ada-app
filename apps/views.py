import datetime
import locale

from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from more_itertools.recipes import grouper
from .models import *
from django.shortcuts import render, get_object_or_404


def home(request):
    blogs = Blogs.objects.filter(status=ACTIVE).order_by('-updated_at')[:4]
    news = News.objects.filter(status=ACTIVE).order_by('-id')[:4]
    places = TravelPlaces.objects.filter(status=ACTIVE)
    gallery = Gallery.objects.get()
    gallery_images = GalleryImage.objects.filter(gallery=gallery)[:6]
    first_gallery = GalleryImage.objects.filter(gallery=gallery).first()
    events = Events.objects.filter(status=ACTIVE).order_by('-id')[:6]
    last_event = Events.objects.filter(status=ACTIVE).order_by("-id").first()
    homes = Home.objects.get()
    practical_informations = PracticalInformation.objects.filter(status=ACTIVE).order_by('id')
    practical_information_items = PracticalInformationItem.objects.filter(status=ACTIVE).order_by('id')
    home_button = HomePageVideo.objects.filter(status=ACTIVE).first()
    museums = Museums.objects.filter(status=ACTIVE)
    return render(request, 'home.html', {
        'home': homes,
        'blogs': blogs,
        'news': news,
        'events': events,
        'last_event': last_event,
        'places': places,
        'galleries': gallery_images,
        'first_gallery': first_gallery,
        'home_button': home_button,
        'practical_informations': practical_informations,
        'practical_information_items': practical_information_items,
        'museums': museums,
    })


# def about(request):
#     abouts = About.objects.first()
#     blogs = Blogs.objects.all()[:4]
#     return render(request, 'about.html', {
#         'abouts': abouts,
#         'blogs': blogs,
#
#     })


def sss(request):
    sss = Sss.objects.filter(is_first=False).order_by('id')
    sss_page = get_object_or_404(SSSPage, slug=request.path.split('/')[2])
    first_sss = Sss.objects.filter(is_first=True).first()
    blogs = Blogs.objects.filter(status=ACTIVE)[:4]
    return render(request, 'sss.html', {
        'sss': sss,
        'sss_page': sss_page,
        'first_sss': first_sss,
        'blogs': blogs,

    })


def my_logo(request):
    my_logos = MyLogo.objects.get()
    if not my_logos:
        return render(request, 'not_found.html')
    blogs = Blogs.objects.filter(status=ACTIVE).order_by('id')[:4]
    return render(request, 'my_logo.html', {
        'my_logo': my_logos,
        'blogs': blogs
    })


def gallery(request):
    gallery = Gallery.objects.get()
    if not gallery:
        return render(request, 'not_found.html')
    gallery_images = GalleryImage.objects.filter(status=ACTIVE, gallery=gallery)
    blogs = Blogs.objects.filter(status=ACTIVE).order_by('id')[:4]
    return render(request, 'gallery.html', {
        'galleries': gallery_images,
        'gallery': gallery,
        'blogs': blogs
    })


def transportation_view(request):
    transportation = Transportation.objects.filter().first()
    if not transportation:
        return render(request, 'not_found.html')
    departure = DepartureTimes.objects.filter(status=ACTIVE)
    return render(request, 'transportation.html', {
        'transportation': transportation,
        'departure': departure
    })


def kullanim_kosullari(request):
    document = Documents.objects.get()
    return render(request, 'kullanim_kosullari.html', {
        'document': document
    })


def gizlilik(request):
    document = Documents.objects.get()
    return render(request, 'gizlilik.html', {
        'document': document
    })


def bilgi_toplumu_hizmetleri(request):
    document = Documents.objects.get()
    return render(request, 'bilgi_toplumu_hizmetleri.html', {
        'document': document
    })


def kisisel_verilerin_korunmasi_kanunu(request):
    document = Documents.objects.get()
    return render(request, 'kisisel_verilerin_korunmasi_kanunu.html', {
        'document': document
    })


def news(request):
    news_page = get_object_or_404(AnnouncementsPage, slug=request.path.split('/')[2])
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    if start_date is None and end_date is None:
        new = News.objects.filter(status=ACTIVE).order_by('-id')[:10]
        blogs = Blogs.objects.filter(status=ACTIVE).order_by('-id')[:4]
        return render(request, 'news.html', {
            'news': new,
            'news_page': news_page,
            'blogs': blogs
        })
    else:
        try:
            results = News.objects.filter(updated_at__range=[start_date, end_date], status=ACTIVE)
        except News.DoesNotExist:
            results = None
        blogs = Blogs.objects.filter(status=ACTIVE).order_by('id')[:4]
        return render(request, 'news.html', {
            'news': results,
            'blogs': blogs,
            'start_date': start_date,
            'end_date': end_date,
        })


def contact(request):
    contacts = Contact.objects.get()
    if not contacts:
        return render(request, 'not_found.html')
    return render(request, 'contact.html', {
        'contact': contacts
    })


@csrf_exempt
def contact_form_view(request):
    if request.method == 'POST':
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        phone = request.POST.get('phone', '')
        message = request.POST.get('message', '')

        created_time = datetime.datetime.now() - datetime.timedelta(seconds=30)
        old_objects = ContactForm.objects.filter(created_at__gte=created_time).count()

        if old_objects < 10:
            res = ContactForm()
            res.name = name
            res.mail = email
            res.phone = phone
            res.kvkk1 = True
            res.kvkk2 = True
            res.message = message
            res.save()

        contacts = Contact.objects.get()
        return render(request, 'contact.html', {
            'contact': contacts
        })


@csrf_exempt
def subscriber_view(request):
    if request.method == 'POST':
        email = request.POST.get('mail', '')

        created_time = datetime.datetime.now() - datetime.timedelta(seconds=30)
        old_objects = Subscriber.objects.filter(created_at__gte=created_time).count()
        if old_objects < 10:
            sub = Subscriber.objects.filter(mail=email).first()
            if not sub:
                res = Subscriber()
                res.mail = email
                res.is_acik_riza_beyani = True
                res.save()

        return HttpResponseRedirect(reverse('home'))


def events(request):
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    if start_date is None and end_date is None:
        event = Events.objects.filter(status=ACTIVE).order_by('-id')[:10]
        return render(request, 'events.html', {
            'events': event
        })
    else:
        try:
            results = Events.objects.filter(updated_at__range=[start_date, end_date], status=ACTIVE).order_by('-id')
        except Events.DoesNotExist:
            results = None
        return render(request, 'events.html', {
            'events': results,
            'start_date': start_date,
            'end_date': end_date,
        })


def panoramic_museum(request):
    panoramic_museums = Blogs.objects.filter(status=ACTIVE).order_by('id')
    return render(request, 'panoramic_museum.html', {
        'panoramic_museum': panoramic_museums
    })


class EventDetail(DetailView):
    model = Events
    template_name = '../templates/event_detail.html'
    slug_field = 'slug'


class NewsDetail(DetailView):
    model = News
    template_name = '../templates/news_detail.html'
    slug_field = 'slug'


class BlogDetail(DetailView):
    model = Blogs
    template_name = '../templates/blog_detail.html'
    slug_field = 'slug'


class MuseumDetail(DetailView):
    model = Museums
    template_name = '../templates/museums.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        museum = self.object

        museum_images = MuseumImage.objects.filter(museum=museum, status=ACTIVE)
        panoramic = PanoramicMuseum.objects.filter(museum=museum, status=ACTIVE)
        events = Events.objects.filter(status=ACTIVE, museum=museum).order_by('-id')[:4]
        last_event = events.first()
        place_rooms = MuseumPlaceRooms.objects.filter(museum=museum, status=ACTIVE).order_by('id')

        place_rooms_group = list(grouper([el for el in place_rooms], 4))

        context['museum'] = museum
        context['galleries'] = museum_images
        context['events'] = events
        context['panoramic'] = panoramic
        context['last_event'] = last_event
        context['place_rooms'] = place_rooms_group

        return context


class PanoramicMuseumDetail(DetailView):
    model = Museums
    template_name = '../templates/panoramic_museum.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        museum = self.object

        museum_images = MuseumImage.objects.filter(museum=museum, status=ACTIVE)
        panoramic = PanoramicMuseum.objects.filter(museum=museum, status=ACTIVE).first()

        context['museum'] = museum
        context['galleries'] = museum_images
        context['panoramic'] = panoramic

        return context


class TravelPlacesDetail(DetailView):
    model = TravelPlaces
    template_name = '../templates/travel_places_detail.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        travel_places = self.object
        travel_place_slider = TravelPlaceSlider.objects.filter(travel_places=travel_places, status=ACTIVE)
        first_gallery = TravelPlaceGallery.objects.filter(travel_places=travel_places, status=ACTIVE).first()
        gallery_images = TravelPlaceGallery.objects.filter(travel_places=travel_places, status=ACTIVE)[:6]
        place_rooms = MuseumPlaceRooms.objects.filter(status=ACTIVE)

        place_rooms_group = list(grouper([el for el in place_rooms], 4))

        context['travel_places'] = travel_places
        context['travel_place_slider'] = travel_place_slider
        context['galleries'] = gallery_images
        context['first_gallery'] = first_gallery
        context['place_rooms'] = place_rooms_group

        return context


class QrCodePageDetail(DetailView):
    model = QrCodePage
    template_name = '../templates/qrcode_page_detail.html'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        qrcode_page = self.object

        context['qrcode_page'] = qrcode_page

        return context


def travel_places_view(request):
    travel_places_page = get_object_or_404(TravelPlacesPage, slug=request.path.split('/')[2])
    travel_places = TravelPlaces.objects.filter(status=ACTIVE)
    travel_place_slider = TravelPlaceSlider.objects.filter(status=ACTIVE)
    first_gallery = TravelPlaceGallery.objects.filter(status=ACTIVE).first()
    gallery_images = TravelPlaceGallery.objects.filter(status=ACTIVE)[:6]
    place_rooms = MuseumPlaceRooms.objects.filter(status=ACTIVE)

    place_rooms_group = list(grouper([el for el in place_rooms], 4))

    return render(request, 'travel_places.html', {
        'travel_places': travel_places.first(),
        'travel_places_page': travel_places_page,
        'travel_place_slider': travel_place_slider,
        'galleries': gallery_images,
        'first_gallery': first_gallery,
        'place_rooms': place_rooms_group,

    })


@never_cache
def clear_cache(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    cache.clear()
    return HttpResponse('Cache has been cleared')