from django.db import models
from django.contrib.auth.models import User
import os

from django.template.defaultfilters import slugify

from apps.helper.pathcreater import get_file_path, get_upload_path
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _

ACTIVE = 1
PASSIVE = 2
PENDING = 2
STATUS_CHOICES = (
    (ACTIVE, 'Active'),
    (PASSIVE, 'Passive'),
)

MUSEUM_TYPE_DEMOCRACY_AND_FREEDOM = 1
MUSEUM_TYPE_ADNAN_MENDERES = 2
MUSEUM_TYPE_HASAN_POLATKAN = 3

MUSEUM_CHOICES = (
    (MUSEUM_TYPE_DEMOCRACY_AND_FREEDOM, 'Demokrasi Ve Özgürlük Müzesi'),
    (MUSEUM_TYPE_ADNAN_MENDERES, 'Adnan Menderes Müzesi Subay Gazinosu'),
    (MUSEUM_TYPE_HASAN_POLATKAN, 'Hasan Polatkan Müzesi Spor Salonu'),
)

INSTAGRAM = 1
TWITTER = 2
LINKEDIN = 3
YOUTUBE = 4
FACEBOOK = 5
GMAIL = 6

SOCIAL_MEDIA_TYPE_CHOICES = (
    (INSTAGRAM, 'INSTAGRAM'),
    (TWITTER, 'TWITTER'),
    (LINKEDIN, 'LINKEDIN'),
    (YOUTUBE, 'YOUTUBE'),
    (FACEBOOK, 'FACEBOOK'),
    (GMAIL, 'GMAIL'),
)


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(_('Kayıt Tarihi'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Güncellenme Tarihi'), auto_now=True)

    class Meta:
        abstract = True


class MetaModelMixin(models.Model):
    meta_title = models.CharField(_('Meta Title'), max_length=100, default='')
    meta_description = models.TextField(_('Meta Description'), default='')
    other_metas = RichTextUploadingField(_('Diğer Metalar'), default='', null=True, blank=True)

    def get_meta_title(self):
        if self.meta_title:
            return self.meta_title

    def get_meta_description(self):
        if self.meta_description:
            return self.meta_description

    class Meta:
        abstract = True


class ImageModelMixin(models.Model):
    image = models.FileField(_('Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')

    class Meta:
        abstract = True


class Home(TimeStampMixin, MetaModelMixin):
    content = RichTextUploadingField(_('İçerik'), blank=True)
    opening_time = models.CharField(_('Açılış Saati'), max_length=100, null=True, blank=True)
    hotel_url = models.URLField(
        _("Hotel Katre Url"),
        max_length=256,
        db_index=True,
        unique=True,
        blank=True
    )
    content_continue = RichTextUploadingField(_('İçerik Devam'), blank=True)

    e_ticket = models.BooleanField(_('E Bilet Durumu'), default=False)
    e_ticket_url = models.URLField(
        _("E Bilet Url"),
        max_length=256,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['-created_at']

        db_table = 'home'
        verbose_name_plural = 'Anasayfa'
        verbose_name = 'Anasayfa'

    def __str__(self):
        return 'Anasayfa'


class PracticalInformation(TimeStampMixin):
    title = models.CharField(_('İsim'), max_length=60, blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'practical_information'
        verbose_name_plural = 'Pratik Bilgiler'
        verbose_name = 'Pratik Bilgiler'

    def __str__(self):
        return self.title


class PracticalInformationItem(TimeStampMixin):
    practical_information = models.ForeignKey(PracticalInformation, default=None, on_delete=models.CASCADE)
    label = models.CharField(_('Metin'), max_length=100, blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        verbose_name_plural = 'Bilgiler'
        verbose_name = 'Bilgi'

    def __str__(self):
        return self.label


class HomePageVideo(TimeStampMixin):
    home = models.ForeignKey(Home, default=None, on_delete=models.CASCADE)
    title = models.CharField(_('Başlık'), max_length=60)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    button_value = models.CharField(_('Buton Adı'), max_length=100, blank=True)
    url = models.CharField(_('Seo'), max_length=100, blank=True)
    link = models.URLField(
        _("Link"),
        max_length=256,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['-created_at']
        verbose_name_plural = 'Ada Video'
        verbose_name = 'Ada Video'

    def __str__(self):
        return self.title


class About(TimeStampMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    images = models.ImageField(_('Resimler'), upload_to=get_file_path, blank=True)
    head_title = models.CharField(_('Üst Başlık'), max_length=100, blank=True)
    thumbnail = RichTextUploadingField(_('Küçük Resim İçerik'), blank=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'about'
        verbose_name_plural = 'Hakkımızda'
        verbose_name = 'Hakkımızda'

    def __str__(self):
        return self.title


class MyLogo(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'my_logo'
        verbose_name_plural = 'Logomuz'
        verbose_name = 'Logomuz'

    def __str__(self):
        return self.title


class Museums(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    opening_time = models.CharField(_('Açılış Saati'), max_length=100, null=True, blank=True)
    pdf = models.FileField(_('PDF'), upload_to=get_upload_path, blank=True)
    slug = models.SlugField(_('Slug'), unique=True, max_length=100, editable=False)
    image = models.ImageField(_('Küçük Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')

    class Meta:
        ordering = ['-created_at']

        db_table = 'museums'
        verbose_name_plural = 'Müzeler'
        verbose_name = 'Müze'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Museums, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class Events(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    museum = models.ForeignKey(Museums, null=True, blank=True, on_delete=models.CASCADE)
    date_range = models.CharField(_('Tarih Aralığı'), max_length=100, blank=True)
    slug = models.SlugField(_('Slug'), unique=True, max_length=100, editable=False)
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    short_description = models.TextField(_('Kısa Açıklama'), max_length=100, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)
    thumbnail_image = models.FileField(_('Küçük Resim'), upload_to=get_file_path)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'Events'
        verbose_name_plural = 'Etkinlikler'
        verbose_name = 'Etkinlik'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Events, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class News(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    slug = models.SlugField(_('Slug'), unique=True, max_length=100, editable=False)
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    short_description = models.TextField(_('Kısa Açıklama'), max_length=100, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)
    thumbnail_image = models.ImageField(_('Küçük Resim'), upload_to=get_file_path)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'news'
        verbose_name_plural = 'Haberler'
        verbose_name = 'Haber'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        # if self.image:
        #     self.image = get_thumbnail(self.image, '500x600', quality=99, format='JPEG')
        super(News, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class MuseumImage(TimeStampMixin, ImageModelMixin):
    museum = models.ForeignKey(Museums, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        verbose_name_plural = 'Resimler'
        verbose_name = 'Resim'

    def __str__(self):
        return 'Resim' + str(self.pk)


class PanoramicMuseum(TimeStampMixin):
    museum = models.ForeignKey(Museums, default=None, on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    image_url = models.URLField(
        _("Url"),
        db_index=True,
        blank=True
    )

    class Meta:
        verbose_name_plural = 'Panoramik Müze'
        verbose_name = 'Panoramik Müze'

    def get_absolute_url(self):
        return f'/{self.image_url}'

    def __str__(self):
        return self.title


class MuseumPlaceRooms(TimeStampMixin, ImageModelMixin):
    museum = models.ForeignKey(Museums, default=None, on_delete=models.CASCADE)
    small_title = models.CharField(_('Küçük Başlık'), max_length=60, blank=True)
    head_title = models.CharField(_('Büyük Başlık'), max_length=60, blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        verbose_name_plural = 'Gezilecek Odalar'
        verbose_name = 'Gezilecek Oda'

    def __str__(self):
        return self.small_title


class TravelPlaces(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    description = RichTextUploadingField(_('İçerik'), blank=True)
    short_description = models.TextField(_('Kısa Açıklama'), blank=True)
    slug = models.SlugField(_('Slug'), unique=True, max_length=100, editable=False)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'travel_places'
        verbose_name_plural = 'Gezilecek Yerler'
        verbose_name = 'Gezilecek Yerler'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(TravelPlaces, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class TravelPlaceSlider(TimeStampMixin):
    travel_places = models.ForeignKey(TravelPlaces, default=None, on_delete=models.CASCADE)
    image = models.FileField(_('Slider Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    short_description = models.TextField(_('Kısa Açıklama'), blank=True)

    class Meta:
        verbose_name_plural = 'Slider'
        verbose_name = 'Slider'

    def __str__(self):
        return self.title


class TravelPlaceGallery(TimeStampMixin):
    travel_places = models.ForeignKey(TravelPlaces, default=None, on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    image = models.FileField(_('Galeri Resim'), upload_to=get_file_path)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        verbose_name_plural = 'Galeri'
        verbose_name = 'Galeri'

    def __str__(self):
        return self.title


class Blogs(TimeStampMixin, MetaModelMixin, ImageModelMixin):
    date_range = models.CharField(_('Tarih Aralığı'), max_length=100)
    slug = models.SlugField(_('Slug'), unique=True, max_length=100, editable=False)
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    short_description = models.TextField(_('Kısa Açıklama'), max_length=100, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)
    thumbnail_image = models.ImageField(_('Küçük Resim'), upload_to=get_file_path)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'blogs'
        verbose_name_plural = 'Bloglar'
        verbose_name = 'Blog'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Blogs, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return f'/{self.slug}'

    def __str__(self):
        return self.title


class Gallery(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'gallery'
        verbose_name_plural = 'Albümler'
        verbose_name = 'Albüm'

    def __str__(self):
        return self.title


class GalleryImage(TimeStampMixin, ImageModelMixin):
    gallery = models.ForeignKey(Gallery, default=None, on_delete=models.CASCADE)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        verbose_name_plural = 'Resimler'
        verbose_name = 'Resim'

    def __str__(self):
        return 'Resim' + str(self.pk)


class Transportation(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_("Başlık"), max_length=60)
    description_title = models.CharField(_("İçerik Başlık"), max_length=60)
    description = RichTextUploadingField(_('İçerik'), blank=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'transportation'
        verbose_name_plural = 'Ulaşım'
        verbose_name = 'Ulaşım'

    def __str__(self):
        return self.title


class DepartureTimes(TimeStampMixin):
    transportation = models.ForeignKey(Transportation, default=None, on_delete=models.CASCADE)
    harbor_name = models.CharField(_('Liman Adı'), max_length=100)
    departureInfo = models.TextField(_('Sefer Bilgisi'), null=True, blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    departureTime = RichTextUploadingField(_('Sefer Saatleri'), blank=True)

    class Meta:
        verbose_name_plural = 'Sefer Saatleri'
        verbose_name = 'Sefer Saati'

    def __str__(self):
        return self.harbor_name


class Contact(TimeStampMixin, MetaModelMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    country_code = models.CharField(_('Ülke Kodu'), max_length=5, null=True)
    phone1 = models.CharField(_('Telefon'), max_length=10)
    phone2 = models.CharField(_('Telefon 2'), max_length=10)
    email1 = models.EmailField(_('Eposta'), max_length=50)
    email2 = models.EmailField(_('Eposta 2'), max_length=50)
    map = models.CharField(_('Harita'), max_length=100)

    class Meta:
        ordering = ['-created_at']

        db_table = 'contact'
        verbose_name_plural = 'İletişim'
        verbose_name = 'İletişim'

    def __str__(self):
        return self.title


class ContactForm(TimeStampMixin):
    name = models.CharField(_('Ad Soyad'), max_length=100, blank=True)
    mail = models.EmailField(_('E-posta'), max_length=100, blank=True)
    phone = models.IntegerField(_('Telefon Numarası'), blank=True, help_text='Telefon Numarası Giriniz')
    kvkk1 = models.BooleanField(_('Kvkk Metni Seçili Mi ?'))
    kvkk2 = models.BooleanField(_('Açık Rıza Metni Seçili Mi ?'))
    message = models.TextField(_('Mesaj'), editable=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'contact_form'
        verbose_name_plural = 'İletişim Formu'
        verbose_name = 'İletişim Formu'

    def __str__(self):
        return self.name + " (" + str(self.phone) + ")"


class Subscriber(TimeStampMixin):
    mail = models.EmailField(_('E-posta'), max_length=100, blank=True)
    is_acik_riza_beyani = models.BooleanField(_("Açık Rıza Metni Seçili Mi ?"), default=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'subscriber'
        verbose_name_plural = 'Aboneler'
        verbose_name = 'Abone'

    def __str__(self):
        return self.mail + "Test"


class Social(TimeStampMixin):
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    icon = models.CharField(_('İcon'), max_length=100, blank=True)
    logo = models.ImageField(_('Resimler'), upload_to=get_file_path, blank=True)
    image_alt_text = models.CharField(_('Resim Alt İçerik'), max_length=200, default='Resim Alt text')
    type = models.SmallIntegerField(_('Sosyal Medya Tipi'), choices=SOCIAL_MEDIA_TYPE_CHOICES)
    order_no = models.SmallIntegerField(_('Sıra No'))
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)
    link = models.URLField(
        _("Link"),
        max_length=256,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['id']

        db_table = 'social_media'
        verbose_name_plural = 'Sosyal Medya'
        verbose_name = 'Sosyal Medya'

    def __str__(self):
        return self.title


class Sss(TimeStampMixin):
    questions = models.CharField(_('Soru'), max_length=100)
    answer = RichTextUploadingField(_('Cevap'), blank=True)
    is_first = models.BooleanField(_("İlk Soru Mu ?"), default=False)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['-created_at']

        db_table = 'sss'
        verbose_name_plural = 'Sıkça Sorulan Sorular'
        verbose_name = 'Sıkça Sorulan Sorular'

    def __str__(self):
        return self.questions


class SSSPage(TimeStampMixin, MetaModelMixin):
    slug = models.SlugField(_('Slug'), max_length=40)

    class Meta:
        ordering = ['-created_at']

        db_table = 'sss_page'
        verbose_name_plural = 'SSS'
        verbose_name = 'SSS'

    def __str__(self):
        return 'SSS'


class TravelPlacesPage(TimeStampMixin, MetaModelMixin):
    slug = models.SlugField(_('Slug'), max_length=40)
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'travel_places_page'
        verbose_name_plural = 'Gezilecek Yerler'
        verbose_name = 'Gezilecek Yerler'

    def __str__(self):
        return 'Gezilecek Yerler'


class AnnouncementsPage(TimeStampMixin, MetaModelMixin):
    slug = models.SlugField(_('Slug'), max_length=40)

    class Meta:
        ordering = ['-created_at']

        db_table = 'announcements_page'
        verbose_name_plural = 'Duyurular'
        verbose_name = 'Duyurular'

    def __str__(self):
        return 'Duyurular'


class Documents(TimeStampMixin):
    title = models.CharField(_('Başlık'), max_length=60)
    kvkk = RichTextUploadingField(_("KVKK"))
    kullanim_kosullari = RichTextUploadingField(_("Kullanım Koşulları"))
    acik_riza_beyani = RichTextUploadingField(_("Açık Rıza Beyanı"))
    gizlilik_politikasi = RichTextUploadingField(_("Gizlilik Politikası"))
    bilgi_toplumu_hizmetleri = RichTextUploadingField(_("Bilgi Toplumu Hizmetleri"))

    class Meta:
        ordering = ['-created_at']

        db_table = 'documents'
        verbose_name_plural = 'Belgeler'
        verbose_name = 'Belge'

    def __str__(self):
        return self.title


class SiteLanguages(TimeStampMixin):
    name = models.CharField(_('İsim'), max_length=100, blank=True)
    code = models.CharField(_('Kodu'), max_length=100, blank=True)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=ACTIVE)

    class Meta:
        ordering = ['id']

        db_table = 'site_languages'
        verbose_name_plural = 'Site Dilleri'
        verbose_name = 'Site Dili'

    def __str__(self):
        return self.name


class LiveSupport(TimeStampMixin):
    title = models.CharField(_("Canlı Destek Adı"), max_length=60)
    url = models.CharField(_("Canlı Destek Url"), max_length=60)
    status = models.SmallIntegerField(_('Durum'), choices=STATUS_CHOICES, default=PENDING)

    class Meta:
        ordering = ['-created_at']

        db_table = 'live_support'
        verbose_name_plural = 'Canlı Destek'
        verbose_name = 'Canlı Destek'

    def __str__(self):
        return self.title


class Analytics(TimeStampMixin):
    name = models.CharField(_('İsim'), unique=True, max_length=100, blank=True)
    key = models.CharField(_('Key'), max_length=150, default='')
    link = models.URLField(
        _("Link"),
        max_length=128,
        db_index=True,
        unique=True,
        blank=True
    )

    class Meta:
        ordering = ['id']

        db_table = 'analytics'
        verbose_name_plural = 'Analytics'
        verbose_name = 'Analytics'

    def __str__(self):
        return self.name


class QrCodePage(TimeStampMixin):
    slug = models.SlugField(_('url'), max_length=40)
    title = models.CharField(_('Başlık'), max_length=60, blank=True)
    content = RichTextUploadingField(_('İçerik'), blank=True)

    class Meta:
        ordering = ['-created_at']

        db_table = 'qrcode_page'
        verbose_name_plural = 'QrCode Sayfaları'
        verbose_name = 'QrCode Sayfaları'

    def __str__(self):
        return self.slug
