import datetime

from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse

from apps.models import ACTIVE, News, Events, Blogs, TravelPlaces, PanoramicMuseum, Museums


class StaticViewSitemap(Sitemap):
    def items(self):
        return [
            'home',
            'sss',
            'my_logo',
            'travel-places',
            'gallery',
            'transportation',
            'news',
            'kullanim_kosullari',
            'gizlilik',
            'bilgi_toplumu_hizmetleri',
            'kisisel_verilerin_korunmasi_kanunu',
            'contact',
            'events',
            'panoramic_museum',
        ]

    def location(self, item):
        return reverse(item)


class BlogSitemap(Sitemap):
    def items(self):
        items = Blogs.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class NewsSitemap(Sitemap):
    def items(self):
        items = News.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class EventsSitemap(Sitemap):
    def items(self):
        items = Events.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class TravelPlacesSitemap(Sitemap):

    def items(self):
        items = TravelPlaces.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items


class MuseumsSitemap(Sitemap):

    def items(self):
        items = Museums.objects.filter(status=ACTIVE)

        for item in items:
            item.slug = str('tr/') + item.slug

        return items
