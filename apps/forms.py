from django import forms
from django.core.files.images import get_image_dimensions


def validate_image(image, width, height):
    if image:
        image_width, image_height = get_image_dimensions(image.file)
        if image_width < width or image_height < height:
            raise forms.ValidationError(
                "Resim Değerleri standart ı karşılamıyor. Lütfen en az {}x{} resim yükleyiniz".format(width, height))


class ModelWithImageFieldForm(forms.ModelForm):

    def clean_image(self):
        image = self.cleaned_data["image"]
        validate_image(image=image, width=700, height=400)
        return image

    def clean_thumbnail_image(self):
        image = self.cleaned_data["thumbnail_image"]
        validate_image(image=image, width=300, height=300)
        return image


class AboutForm(forms.ModelForm):

    def clean_image(self):
        image = self.cleaned_data["image"]
        validate_image(image=image, width=500, height=500)
        return image


class ModelWithImageFieldForm2(forms.ModelForm):

    def clean_image(self):
        image = self.cleaned_data["image"]
        validate_image(image=image, width=840, height=450)
        return image

    def clean_thumbnail_image(self):
        image = self.cleaned_data["thumbnail_image"]
        validate_image(image=image, width=500, height=500)
        return image


class EventsForm(forms.ModelForm):

    def clean_image(self):
        image = self.cleaned_data["image"]
        validate_image(image=image, width=700, height=400)
        return image

    def clean_thumbnail_image(self):
        image = self.cleaned_data["thumbnail_image"]
        validate_image(image=image, width=200, height=200)
        return image


class BlogsForm(forms.ModelForm):

    def clean_image(self):
        image = self.cleaned_data["image"]
        validate_image(image=image, width=700, height=400)
        return image

    def clean_thumbnail_image(self):
        image = self.cleaned_data["thumbnail_image"]
        validate_image(image=image, width=200, height=200)
        return image