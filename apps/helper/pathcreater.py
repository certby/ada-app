from __future__ import unicode_literals

from os.path import join, splitext


def get_file_path(instance, filename):
    from apps.models import Home, About, MyLogo, TravelPlaces, TravelPlaceSlider, TravelPlaceGallery, Events, News, Museums, \
        Blogs, GalleryImage, \
        Gallery, MuseumImage, MuseumPlaceRooms, PanoramicMuseum
    lastPath = splitext(filename)[1][1:].lower()
    instanceType = type(instance)

    if instanceType is Home:
        return 'home/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is About:
        return 'about/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MyLogo:
        return 'my_logo/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is TravelPlaces:
        return 'travel_places/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is TravelPlaceSlider:
        return 'travel_place_slider/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is TravelPlaceGallery:
        return 'travel_place_gallery/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Events:
        return 'events/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is PanoramicMuseum:
        return 'panoramic/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is News:
        return 'news/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Museums:
        return 'museums/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MuseumPlaceRooms:
        return 'museum_place_room/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Blogs:
        return 'blogs/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is Gallery:
        return 'gallery/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is GalleryImage:
        return 'gallery_image/{}/image/{}'.format(str(instance.id), filename)
    elif instanceType is MuseumImage:
        return 'museum_image/{}/image/{}'.format(str(instance.id), filename)
    return join(instance, filename)


def get_upload_path(instance, filename):
    from apps.models import Museums
    instanceType = type(instance)

    if instanceType is Museums:
        return 'museums/{}/file/{}'.format(str(instance.id), filename)
    return join(instance, filename)
