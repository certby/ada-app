from django import template

from apps.models import Social, Documents, Home, LiveSupport, SiteLanguages, ACTIVE

register = template.Library()


@register.simple_tag()
def social_media():
    return Social.objects.all()


@register.simple_tag()
def document_media():
    return Documents.objects.get()


@register.simple_tag()
def opening_time():
    home = Home.objects.get()
    return home.opening_time


@register.simple_tag()
def e_ticket():
    home = Home.objects.get()
    return home.e_ticket


@register.simple_tag()
def e_ticket_url():
    home = Home.objects.get()
    return home.e_ticket_url


@register.simple_tag()
def live_supports():
    return LiveSupport.objects.filter(status=ACTIVE).first()


@register.simple_tag()
def get_site_languages():
    return SiteLanguages.objects.filter(status=ACTIVE)
