from django.contrib import admin
from django.contrib.admin import TabularInline, StackedInline
from django.contrib.auth.models import Group
from django.forms import Textarea
from jet.admin import CompactInline
from jet.filters import DateRangeFilter

from .forms import ModelWithImageFieldForm, EventsForm, BlogsForm
from .models import *
from modeltranslation.admin import TranslationAdmin
from sitetree.admin import TreeItemAdmin, override_item_admin

# admin.site.register(About)
admin.site.register(Social)
admin.site.unregister(Group)


@admin.register(LiveSupport)
class LiveSupportAdmin(admin.ModelAdmin):
    exclude = ['title', ]


@admin.register(SiteLanguages)
class SiteLanguagesAdmin(admin.ModelAdmin):
    list_display = ['name', 'code', 'status']
    readonly_fields = ['name', 'code']

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CustomTreeItemAdmin(TreeItemAdmin, TranslationAdmin):
    verbose_name = 'MENU'
    verbose_name_plural = 'MENULER'
    # exclude = ('title_en', 'title_tr', 'url_tr', 'url,en')
    """This allows admin contrib to support translations for tree items."""


override_item_admin(CustomTreeItemAdmin)


class GalleryImageAdmin(CompactInline):
    model = GalleryImage
    extra = 0
    verbose_name = 'Resim'
    verbose_name_plural = 'Resimler'


class HomePageVideoAdmin(StackedInline):
    model = HomePageVideo
    extra = 0

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'link',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'button_value_tr',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'button_value_en',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'button_value_ar',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'button_value_ru',),
            'classes': ('tab-fs-content',),
        }),
    )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class PracticalInformationItemAdmin(CompactInline):
    model = PracticalInformationItem
    extra = 0
    verbose_name = 'Bilgi'
    verbose_name_plural = 'Bilgiler'
    exclude = ['label', ]


class PanoramicMuseumAdmin(StackedInline):
    model = PanoramicMuseum
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(ContactForm)
class ContactFormAdmin(admin.ModelAdmin):
    list_display = ('name', 'mail', 'phone', 'kvkk1', 'kvkk2', 'created_at')
    search_fields = ['mail', 'name', 'message']
    list_filter = (
        ('created_at', DateRangeFilter),
    )

    class Meta:
        model = ContactForm

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    list_display = ('mail', 'is_acik_riza_beyani', 'created_at')

    class Meta:
        model = Subscriber

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    form = EventsForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    list_display = ('title', 'status', 'created_at', 'updated_at')

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'museum', 'date_range', 'image', 'image_alt_text', 'thumbnail_image',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'short_description_tr', 'content_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'short_description_en', 'content_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'short_description_ar', 'content_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'short_description_ru', 'content_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )


@admin.register(Blogs)
class BlogsAdmin(admin.ModelAdmin):
    form = BlogsForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    list_display = ('title', 'status', 'created_at', 'updated_at')

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'date_range', 'image', 'image_alt_text', 'thumbnail_image',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'short_description_tr', 'content_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'short_description_en', 'content_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'short_description_ar', 'content_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'short_description_ru', 'content_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    list_display = ('title', 'status', 'created_at', 'updated_at')

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'image', 'image_alt_text', 'thumbnail_image',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'short_description_tr', 'content_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'short_description_en', 'content_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'short_description_ar', 'content_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'short_description_ru', 'content_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    class Meta:
        model = News


@admin.register(Home)
class HomeAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [HomePageVideoAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('opening_time', 'hotel_url', 'e_ticket', 'e_ticket_url',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('content_tr', 'content_continue_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('content_en', 'content_continue_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('content_ar', 'content_continue_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('content_ru', 'content_continue_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    class Meta:
        model = Home


@admin.register(SSSPage)
class SSSPageAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm

    fieldsets = (
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(AnnouncementsPage)
class AnnouncementsPageAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm

    fieldsets = (
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [GalleryImageAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('title',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(Documents)
class DocumentsAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('Kvkk', {
            'fields': ('kvkk_tr', 'kvkk_en', 'kvkk_ar', 'kvkk_ru'),
        }),
        ('Açık Rıza Beyanı', {
            'fields': ('acik_riza_beyani_tr', 'acik_riza_beyani_en', 'acik_riza_beyani_ar', 'acik_riza_beyani_ru'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('Gizlilik Politikası', {
            'fields': (
                'gizlilik_politikasi_tr', 'gizlilik_politikasi_en', 'gizlilik_politikasi_ar', 'gizlilik_politikasi_ru'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('Bilgi Toplumu Hizmetleri', {
            'fields': ('bilgi_toplumu_hizmetleri_tr', 'bilgi_toplumu_hizmetleri_en',
                       'bilgi_toplumu_hizmetleri_ar', 'bilgi_toplumu_hizmetleri_ru'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('Kullanım Koşulları', {
            'fields': ('kullanim_kosullari_tr', 'kullanim_kosullari_en', 'kullanim_kosullari_ar',
                       'kullanim_kosullari_ru'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),

    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(PracticalInformation)
class PracticalInformationAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [PracticalInformationItemAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru',),
            'classes': ('tab-fs-content',),
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(MyLogo)
class MyLogoAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('İçerik (TR)', {
            'fields': ('title_tr', 'content_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'content_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'content_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'content_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Sss)
class SssAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'is_first'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('questions_tr', 'answer_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('questions_en', 'answer_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('questions_ar', 'answer_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('questions_ru', 'answer_ru'),
            'classes': ('tab-fs-content',),
        })
    )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('Genel', {
            'fields': ('country_code', 'phone1', 'phone2', 'email1', 'email2', 'map'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar',),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru',),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


class MuseumImageAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = MuseumImage
    extra = 0


class MuseumPlaceRoomsAdmin(CompactInline):
    model = MuseumPlaceRooms
    extra = 0


@admin.register(TravelPlacesPage)
class TravelPlacesPageAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (

        ('İçerik (TR)', {
            'fields': ('title_tr', 'content_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'content_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'content_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'content_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('slug', 'meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Museums)
class MuseumsAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [MuseumImageAdmin, MuseumPlaceRoomsAdmin, PanoramicMuseumAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'opening_time', 'pdf', 'image'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'content_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'content_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'content_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'content_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    class Meta:
        model = Museums


@admin.register(QrCodePage)
class QrCodePageAdmin(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('Genel', {
            'fields': ('slug',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik', {
            'fields': ('title', 'content'),
            'classes': ('tab-fs-content',),
        })
    )


class TravelPlaceSliderAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = TravelPlaceSlider
    extra = 0
    verbose_name = 'Resim'
    verbose_name_plural = 'Slider'


class TravelPlaceGalleryAdmin(CompactInline):
    form = ModelWithImageFieldForm
    model = TravelPlaceGallery
    extra = 0
    verbose_name = 'Resim'
    verbose_name_plural = 'Galeri'


@admin.register(TravelPlaces)
class TravelPlaces(admin.ModelAdmin):
    form = ModelWithImageFieldForm
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }
    inlines = [TravelPlaceSliderAdmin, TravelPlaceGalleryAdmin]

    fieldsets = (
        ('Genel', {
            'fields': ('status', 'image', 'image_alt_text',),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
        ('İçerik (TR)', {
            'fields': ('title_tr', 'description_tr', 'short_description_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'description_en', 'short_description_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'description_ar', 'short_description_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'description_ru', 'short_description_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False


class DepartureTimesAdmin(CompactInline):
    model = DepartureTimes
    extra = 1


@admin.register(Transportation)
class TransportationAdmin(admin.ModelAdmin):
    inlines = [DepartureTimesAdmin]
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'style': 'font-size: 16px;'})},
    }

    fieldsets = (
        ('İçerik (TR)', {
            'fields': ('title_tr', 'description_tr', 'description_title_tr'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (EN)', {
            'fields': ('title_en', 'description_en', 'description_title_en'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (AR)', {
            'fields': ('title_ar', 'description_ar', 'description_title_ar'),
            'classes': ('tab-fs-content',),
        }),
        ('İçerik (RU)', {
            'fields': ('title_ru', 'description_ru', 'description_title_ru'),
            'classes': ('tab-fs-content',),
        }),
        ('SEO', {
            'fields': ('meta_title', 'meta_description', 'other_metas',),
            'classes': ('tab-fs-tech',),
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(Analytics)
class AnalyticsAdmin(admin.ModelAdmin):
    readonly_fields = ['name', ]

    fieldsets = (
        ('Genel', {
            'fields': ('name', 'key', 'link'),
            'classes': ('baton-tabs-init', 'baton-tab-inline-attribute', 'baton-tab-fs-content',
                        'baton-tab-group-fs-tech--inline-feature',),
        }),
    )

    # keep only view permission
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
