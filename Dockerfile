FROM python:3.6

RUN mkdir /src

WORKDIR /src

COPY ./ /src/

COPY manage.py gunicorn-cfg.py requirements.txt .env /src/

RUN apt-get update -y
RUN apt-get -y install nano

RUN pip install -r requirements.txt

ENV LANG tr_TR.UTF-8
ENV LC_ALL tr_TR.UTF-8

RUN mkdir /root/logs

RUN chmod a+rw db.sqlite3

VOLUME db.sqlite3

EXPOSE 80
CMD ["gunicorn", "--config", "gunicorn-cfg.py", "app.wsgi"]