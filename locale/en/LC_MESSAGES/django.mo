��    V      �     |      x     y  	   �     �     �  
   �     �     �  !   �     �  8   �  
   7     B     S     \     u     �  *   �  "   �     �     �  #   	     /	  D   O	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     .
     ?
     P
     j
     s
     �
     �
     �
     �
     �
     �
     �
     �
                    &  
   -  $   8     ]     s     �     �     �  
   �     �     �     �          
     #  W   3     �     �     �     �  	   �  
   �     �     �     �     �       
              .     H     [  
   c     n  Y  w     �  
   �     �  	   
  	             +  *   7     b  2   o     �     �     �     �  	   �  #   �  ,     #   9     ]     b     p     �     �     �     �     �     �     �     �            	         *  	   9     C     V     f     v     �     �     �     �     �     �     �     �     �     �     �     �     �          
          /     <     L     ^     g     }     �     �     �     �     �     �  9   �     1     L  
   P     [     a     i     v     {     �     �     �     �  
   �     �     �     �     �     �                <   :                  7   4   5            
   ,       E       H           Q      K      8   )   1   J              U       P                                  M              D   >      S   G   A   !                 3               C          +       .       L       @   $         ?   R   *   	       2          9   /          I                 0   -   V          6   F      '   "   #         =   %       N   B             &          ;   T          (           O    360 MÜZEYE GİT 360 Müze ADAYA ULAŞIM Abone Ol Abone Olun Ad Soyad Ada Haberler Aydınlatma metni okudum anladım AÇILIŞ SAATİ Açık Rıza Beyanını okudum, anladım ve onaylıyorum Açıklama Açılış Saati Başlık Bilgi Toplumu Hizmetleri BİZİ TAKİP EDİN DEMOKRASİ VE ÖZGÜRLÜK ADASI DEMOKRASİ VE ÖZGÜRLÜK ADASI NEREDEDİR DEMOKRASİ VE ÖZGÜRLÜK MÜZESİ DEVAMI Demokrasi ve Demokrasi ve <br>Özgürlük Adası Demokrasi ve Özgürlük Adası Demokrasi ve Özgürlükler Adası - 2020 / Tüm Hakları Saklıdır Devamı Durum E-BİLET E-POSTA ADRESİ E-posta Adresi E-posta Adresinizi Girin Eposta Eposta 2 Etkinliği Gör GEZİLECEK YERLER Galeri Galeri & Videolar Gezilecek Odalar Gezilecek Yerler Gezilecek Yerler & Odalar Gizlilik Gizlilik Politikası GÖSTER GÜNCEL ETKİNLİKLER Gönder Güncel Etkinlikler Güncel Haberler HABERLERİN TAMAMI HAFTA SONU KAPALI HAFTA İÇİ Haberi Gör Haberler Hakımızda Harita Her gün:  Kişisel Verilerin Korunması Kanunu Kullanım Koşulları Küçük Resim İçerik Kısa Açıklama Logomuz Logomuzun Hikayesi Müze Tipi Müzelerimiz PRATİK BİLGİLER Pazartesi ve Tatiller: Kapalı Resimler Röportajlar & Yayınlar SEFER SAATLERİ Sergilerimiz, etkinliklerimiz ve daha fazlası hakkında e-posta güncellemeleri alın: Sıkça Sorulan Sorular TAMAMINI Tarih Aralığı Telefon Telefon 2 Telefon No Tipi Ulaşım YAKINDA Ziyaret Bilgileri Özgürlük Adası Ülke Kodu Üst Başlık İLETİŞİM BİLGİLERİ İLETİŞİM FORMU İNDİR İletişim İçerik Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-03-14 13:29+0156
Last-Translator: b'  <>'
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Translated-Using: django-rosetta 0.9.5
 360 MUSEUM TO GO 360 Museum TRANSPORTATION TO THE ISLAND Subscribe Subscribe Name Surname Island News I read the clarification text I understood OPENING TIME I have acknowledged the Explicit Consent Statement Description Opening time Title Information Society Services FOLLOW US DEMOCRACY AND THE ISLAND OF FREEDOM WHERE IS DEMOCRACY AND THE ISLAND OF FREEDOM DEMOCRACY AND THE ISLAND OF FREEDOM MORE Democracy and Democracy and Freedom Island Democracy and Freedom Island Democracy and Freedom Island More Status E-TICKET EMAİL ADDRESS Email Address Enter Your Email Address Email Email 2 See Event PLACE TO VISIT Galleries Galleries & Videos Places to Visit Places to Visit Places to Visit Privacy Privacy Policy SHOW CURRENT EVENTS Send Current Events Recent News ALL NEWS OFF WEEKEND WEEKDAYS See News News About Us Map Every Day:  Personal Data Protection Terms of use Thumbnail Image Short Description Our Logo The Story of Our Logo Museum Type Our museums PRACTICAL INFORMATION Mondays and Holidays: Closed Images Interviews & Publications OPENING HOURS Receive email updates on our exhibitions, events and more Frequently Asked Questions ALL Date Range Phone Phone 2 Phone Number Type Transportation SOON Visit Information Freedom Island Country Code Head Title CONTACT INFORMATION CONTACT FORM DOWNLOAD Contact Content 